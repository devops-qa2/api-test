package tests;

import io.restassured.response.Response;
import service.BaseService;

public class BaseTest {

    /*
    * Responses personalizadas
    * */
    protected BaseService baseService;
    protected Response response;

    public BaseTest(){
        baseService = new BaseService();
    }

}
