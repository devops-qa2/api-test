package tests;

import data.factory.TaskFactory;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TaskTest extends BaseTest {

    @Test
    @DisplayName("Teste deve retornar tarefas")
    public void testDeveRetornarTarefas() {
        response = baseService.get("/todo");

        response.then().statusCode(HttpStatus.SC_OK);

    }

    @Test
    @DisplayName("Teste deve adicionar tarefa com sucesso")
    public void testDeveAdicionarTarefaComSucesso() {

        response = baseService.post("/todo", TaskFactory.comDadosValidos());

        response.then().statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    @DisplayName("Teste nao deve adicionar tarefa invalida")
    public void testNaoDeveAdicionarTarefaComSucesso() {

        baseService.post("/todo", TaskFactory.comDadosInvalidos())
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("message", Matchers.is("Due date must not be in past"));

    }
}
