package specs;

public interface ISpecBuilder<Service> {
    Service withAuth(String token);
}
