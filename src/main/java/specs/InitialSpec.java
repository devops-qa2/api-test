package specs;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.specification.RequestSpecification;
import utils.UtilsProperties;

import static io.restassured.filter.log.LogDetail.ALL;
import static io.restassured.http.ContentType.JSON;

public class InitialSpec {

    public static RequestSpecBuilder requestSpecBuilder() {

        SSLConfig sslConfig = new SSLConfig()
                .relaxedHTTPSValidation();

        LogConfig logConfig = new LogConfig()
                .enablePrettyPrinting(true)
                .enableLoggingOfRequestAndResponseIfValidationFails(ALL);

        RestAssuredConfig restAssuredConfig = new RestAssuredConfig()
                .sslConfig(sslConfig)
                .logConfig(logConfig);

        return new RequestSpecBuilder()
                .setConfig(restAssuredConfig)
                .setRelaxedHTTPSValidation()
                .setContentType(JSON)
                .setAccept(JSON)
                .setBaseUri(UtilsProperties.getBaseURL());
    }

    public static RequestSpecification setup() {
        return requestSpecBuilder().build();
    }
}
