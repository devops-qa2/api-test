package specs;

import io.restassured.specification.RequestSpecification;
import org.apache.http.auth.AUTH;

public class AuthSpec {
    public static RequestSpecification setup(String token) {
        return InitialSpec.requestSpecBuilder()
                .addHeader(AUTH.WWW_AUTH_RESP, token)
                .build();
    }
}
