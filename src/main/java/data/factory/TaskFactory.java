package data.factory;

import data.dto.task.Task;

public class TaskFactory {

    public static Task comDadosValidos(){
        return Task.builder()
                .task("adicionar task com sucesso")
                .dueDate("2024-10-10")
                .build();
    }

    public static Task comDadosInvalidos(){
        return Task.builder()
                .task("adicionar task com sucesso")
                .dueDate("2010-01-01")
                .build();
    }
}
