package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class UtilsProperties {

    private final static String PROPERTIES_PATH = "src/main/resources/environment.properties";

    public static String getBaseURL(){
        if(System.getProperty("BASE_URL") == null)
            return getProperties().getProperty("BASE_URL");
        return System.getProperty("BASE_URL");
    }

    private static Properties getProperties() {
        Properties propsResponse = new Properties();
        try {
            propsResponse.load(new FileInputStream(PROPERTIES_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return propsResponse;
    }
}
