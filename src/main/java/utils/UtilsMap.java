package utils;

import java.util.HashMap;
import java.util.Map;

public class UtilsMap {
    @SafeVarargs
    public static Map<String, String> mergeMap(Map<String, String>... maps) {
        Map<String, String> mergeMap = new HashMap<>();
        for (Map<String, String> map : maps) {
            mergeMap.putAll(map);
        }
        return mergeMap;
    }

    public static Map<String, String> param(String param, String valor)
    {
        return new HashMap<>(Map.of(param, valor));
    }
}
