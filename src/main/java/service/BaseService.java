package service;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import specs.InitialSpec;

import java.util.Map;

public class BaseService {
    public RequestSpecification spec;

    public BaseService(){
        spec = InitialSpec.setup();
    }

    public void setSpec(RequestSpecification spec){
        this.spec = spec;
    }

    public Response get(String endPoint) {
        return given()
                    .spec(spec)
                .when()
                    .get(endPoint);
    }

    public Response getQueryParams(String endPoint, Map<String, String> params){
        return given()
                    .spec(spec)
                .when()
                    .queryParams(params)
                    .get(endPoint);
    }

    public Response getPathParams(String endPoint, Map<String, String> params){
        return given()
                    .spec(spec)
                .when()
                    .pathParams(params)
                    .get(endPoint);
    }


    public Response post(String endPoint, Object body) {
        return given()
                    .spec(spec)
                    .body(body)
                .when()
                    .post(endPoint);
    }

    public Response update(String endPoint, Object body, Map<String, String> params) {
        return given()
                    .spec(spec)
                    .pathParams(params)
                    .body(body)
                .when()
                    .put(endPoint);
    }

    public Response delete(String endPoint, Map<String, String> params) {
        return given()
                    .spec(spec)
                    .pathParams(params)
                .when()
                    .delete(endPoint);

    }


}
